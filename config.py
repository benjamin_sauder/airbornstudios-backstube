supportedFormats = (".obj", ".sbm", ".3ds", ".x", ".dxf", ".fbx", ".lwo", ".lxo",  )

lowPrefixes = ("lo_", "low_", "lo.", "low.")
highPrefixes = ("hi_", "high_", "hi.", "high." )
cagePrefixes = ("ca_", "cage_", "ca.", "cage.")

generatedXmlPrefix = "xN_"

unityTangentBasis = "{0669A8DD-A5B6-41AD-A215-2EDAC02BD4A9}"
mikkTangentBasis  = "{887AEA64-888A-43FD-B087-6F64025F65B9}"
tangentBasis = mikkTangentBasis

isDebug = False;