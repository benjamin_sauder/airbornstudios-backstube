#Airborn Studios - Backstube
a small utility application for xNormal


![screenshot](https://bitbucket.org/benjamin_sauder/airbornstudios-backstube/raw/7625738b784b2430578beda028e3214428891b56/screenshot.png)


##Core-Concept:
The whole application is based on the idea to use xNormal settings files
as a blueprint for baking an arbitrary number of highpoly and lowpoly meshes.
Or even better an arbitrary number of setting files with any meshes you want.
This really speeds up the process, if you find yourself switching models and output 
in xNormal all the time, but never really touch the settings themselves.

##Requirements:
* python 3.3+
* xNormal 3.0+

##Quick step by step guide:

1.	Start the application, and supply it with a valid xNormal install directory
1.	Setup xNormal with one highpoly and one lowpoly mesh, define the maps and all other stuff
1.	Save these settings somewhere, you can setup multiple settings files too if you need some 
	different things.
1.	Close xNormal
1.	First, export all your highpoly models with a prefix __"hi_"__ or __"high_"__	
	Second, export all your lowpoly models with a prefix __"lo_"__ or __"low_"__
 	
	To avoid intersections you can use a simple folder structure, these get baked one after another.
	
	Example structure:  
		* lo_gun.sbm  
		* hi_gun.obj  
		* hi_trigger.obj  
		* hi_scope.obj  
		* Magazine - lo_mag.sbm  
		* Magazine - hi_mag.obj  
		* Magazine - hi_ammo.obj
  
	
1.	Doubleclick on Backstube.pyw ( if nothing happes try, assocation it with the pythonw.exe in the python install directory)
	If it still doesnt run, type cmd in the explorer and start the Launcher.pyw from the commandshell, and look	what it says - and contact me
	
1.	Copy&Paste path to modelsfolder into the "Model Directory" entryfield

1.	Copy&Paste path to settings files into the "Settings Directory" entryfield

1.	(optional) setup a destination folder for your baked maps if you do not specify this, everything gets saved into the model directory

1.	Hit "Bake Now" and wait...

Have a lookt at the TestData folder - and try to bake that one first.

The tool generates "xN_'your_settings_filename'.xml" files in each of your modelfolders,
these get rewritten every time you click on bake now!

----

its licencend under BSD see licence.md



