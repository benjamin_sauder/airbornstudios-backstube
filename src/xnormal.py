import os
import sys
import xml.etree.ElementTree as ET
import subprocess

from config import *

path = ""
settings = []
bakeParts = []

def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

def isValidXmlFile(path):
    return path[-4:] == ".xml" and path[:3] != generatedXmlPrefix

def getBaseName(s, prefixes):
    for prefix in prefixes:
        if s.startswith(prefix):
            return s[len(prefix):-4]


def collectSettingsAndModels(settingsPath, modelPath):
    global settings, bakeParts
    settings.clear()

    if os.path.isdir(settingsPath):
        for root, dirs, files in walklevel(settingsPath, 0):
            settings = list(filter(isValidXmlFile, files))
            settings = list(map(lambda x: os.path.join(root, x), settings))
    elif os.path.isfile(settingsPath) and isValidXmlFile(settingsPath):
        settings.append(settingsPath)

    if len(settings) == 0:
        raise Exception("Error: no xNormal settings files found!")

    bakeParts.clear()
    for root, dirs, files in walklevel(modelPath):
        low = list(filter(lambda x: x.lower().startswith(lowPrefixes) and x[-4:].lower() in supportedFormats, files))
        high = list(filter(lambda x: x.lower().startswith(highPrefixes) and x[-4:].lower() in supportedFormats, files))

        cage_models = list(filter(lambda x: x.lower().startswith(cagePrefixes) and x[-4:].lower() in supportedFormats, files))
        cage_mapping = {}
        resolution = None

        for model in low:
            modelBaseName = getBaseName(model, lowPrefixes)
            
            for cage in cage_models:
                if getBaseName(cage, cagePrefixes) == modelBaseName:
                    cage_mapping[model] = cage
                    print("found cage for: %s" % (model))
                    break    

            if "@" not in modelBaseName: continue
            resolutionString = modelBaseName[ modelBaseName.find("@")+1 : ]                      
            try:
                resolution = list(map( int, resolutionString.split("x") ))              
            except:
                resolution = None               

        if len(low) == 0 or len(high) == 0:
            continue

        bakeParts.append({"root": root, "low": low, "high": high, "cage_mapping" : cage_mapping, "resolution" : resolution})

    if len(bakeParts) == 0:
        raise Exception("Error: no meshes found!")


def generateXML(texturePath, modelPath):
    readyToBake = []

    for setting in settings:
        for part in bakeParts:

            try:
                tree = ET.parse(setting)
                root = tree.getroot()
            except Exception:
                raise Exception("Error: parsing XML Settingsfile failed - %s" %
                                (os.path.split(setting)[1]))

            def setModelData(xmlNode, meshKey):
                parent = root.find(xmlNode)
                if parent == None:
                    raise Exception(
                        "Error: no valid xnormal settings file - %s" % (os.path.split(setting)[1]))

                meshes = parent.findall("Mesh")
                # cleanup if more than one hipoly mesh is in settings
                if len(meshes) > 1:
                    for i in range(len(meshes)):
                        if i == 0:
                            continue
                        parent.remove(meshes[i])

                try:
                    for i in range(len(part[meshKey]) - 1):
                        ET.SubElement(parent, "Mesh", parent.find("Mesh").attrib)
                except Exception:
                    raise Exception(
                        "Error: no %s entry found in settings file - %s" % (meshKey, os.path.split(setting)[1]))

                meshes = parent.findall("Mesh")
                for i in range(len(meshes)):
                    meshes[i].set("File", os.path.join(part["root"], part[meshKey][i]))

                    #add cages on the lowpolies if there are any
                    try:
                        if meshes[i].get("UseCage") == "true":
                            cage = part["cage_mapping"][part[meshKey][i]]
                            meshes[i].set("CageFile", os.path.join(part["root"], cage))
                        
                    except:                        
                        pass                      

            setModelData("HighPolyModel", "high")
            setModelData("LowPolyModel", "low")

            # maps
            maps = root.find("GenerateMaps")
            if maps == None:
                raise Exception("Error: no valid xnormal settings file - %s" %
                                (os.path.split(setting)[1]))

            path, directory = os.path.split(part["root"])
            # if stuff lies around in root folder no need to add part subname
            if part["root"] == modelPath:
                directory = ""
            else:
                directory = "_" + directory

            bakeImageFormat = os.path.split(maps.get( "File"))[1][-4:]            
            mapname = os.path.split(setting)[1][:-4] + directory + bakeImageFormat          
            maps.set("File", os.path.join(texturePath, mapname))
            #print( maps.get("File") )

            if part["resolution"] != None:
                maps.set("Width",  str(part["resolution"][0]) )
                maps.set("Height", str(part["resolution"][1]) ) 

            # save file
            xml = os.path.join(part["root"], "%s%s.xml" % (generatedXmlPrefix, os.path.split(setting)[1][:-4]))
            
            print("writing: " + xml)
            try:
                tree.write(xml)
                readyToBake.append(xml)
            except Exception:
                raise Exception("Error: could not write XML - %s" % (xml))            

    return readyToBake


def deleteXMLFiles(xmlFiles):
    for xml in xmlFiles:
        try:
            os.remove(xml)
        except:
            pass


def batch(settingsFolder, modelFolder, outDir="", keepXml=True ):
    '''
    main worker method, kicks off all tasks
    settingsFolder -> a folder with the xnormal setting file or multiple of em
    modelFolder -> base folder where model search starts, it looks one level deep for meshes
    outDir -> an optional path where the baked texturemaps are saved
    '''
    settingsPath = os.path.abspath(settingsFolder)
    modelPath = os.path.abspath(modelFolder)

    if outDir:
        texturePath = os.path.abspath(outDir)
    else:
        texturePath = modelPath

    if os.path.exists(settingsPath) and os.path.isdir(modelPath) and os.path.isdir(texturePath):
        collectSettingsAndModels(settingsPath, modelPath)
        toBake = generateXML(texturePath, modelPath)
    else:
        raise Exception("Error: invalid Folder Paths supplied")

    # set tangent space
    setRegistry( tangentBasis )

    # call xnormal
    retcode = subprocess.call("%s %s" % (path,  '"' + '" "'.join(toBake) + '"'))

    if not keepXml:
        deleteXMLFiles(toBake)

    return retcode

def setRegistry( value ):
    import winreg    
    keyVal = r'Software\Santiago Orgaz\xNormal v3'
    try:
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, keyVal, 0, winreg.KEY_ALL_ACCESS)
    except:
        key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, keyVal)
    winreg.SetValueEx(key, "CurrentTB", 0, winreg.REG_SZ, value )
    winreg.CloseKey(key)

def main():
    '''
    to work from commandline, this needs the following arguments:
    path to xnormal.exe
    path to settingsfolder
    path to modelfolder
    path to outputfolder ( this is optional - the maps get saved into the modelfolder)
    '''
    argv = sys.argv

    if(len(argv) < 4):
        raise Exception("Error: invalid arguments!")

    global path
    path = os.path.abspath(argv[1])

    if os.path.isfile(path):
        if(len(argv) == 3):
            batch(argv[2], argv[3], argv[3])
        else:
            batch(argv[2], argv[3], argv[4])
    else:
        raise Exception("Wrong xNormal.exe path supplied?")

if __name__ == '__main__':
    main()
