from tkinter import *
from imagebutton import *
import gui_colors
import os
import pickle
import xnormal

from config import isDebug


version = 1
config = {"version": version, "modelpath": "c:/", "settinspath": "c:/",
          "outDir": "(optional)", "xnormalPath": "NOT SET", "keepXml": True}


class GUI():

    def __init__(self, master):
        global config
        try:
            with open('config.pickle', 'rb') as f:
                configToValidate = pickle.load(f)
                if configToValidate["version"] == version:
                    config = configToValidate
                else:
                    config["xnormalPath"] = configToValidate["xnormalPath"]
                    print("old config - revert to defaults")
        except Exception as e:
            #raise e
            print(e)

        self.master = master

        self.mainFrame = Frame(master)
        self.mainFrame["background"] = gui_colors.darkBlue
        self.mainFrame.pack(fill=BOTH, expand=1)

        self.bgImage = PhotoImage(file="res/bg.pgm")
        self.background = Label(
            self.mainFrame, image=self.bgImage, borderwidth=0)
        self.background.place(relx=0, rely=0, anchor=NW)

        self.warnLabel = Label(self.mainFrame,
                               foreground=gui_colors.warning, bg=gui_colors.darkBlue, text="")
        self.warnLabel.place(relx=0.985, rely=1.01, anchor=SE)

        # buttons
        self.settingsButton = ImageButton(
            self.mainFrame, "settings", self.showSettings)
        self.settingsButton.place(relx=0.845, rely=0.3, anchor=CENTER)

        self.IMG_empty = PhotoImage(file="res/empty.pgm")
        self.empty = Label(
            self.mainFrame, image=self.IMG_empty, borderwidth=0,
            highlightthickness=0, activebackground=gui_colors.darkBlue)
        self.empty.place(relx=0.82, rely=0.5, anchor=NW)

        self.bakeButton = ImageButton(self.mainFrame, "bake", self.bake)
        self.bakeButton.place(relx=0.93, rely=0.48, anchor=CENTER)

        # entry fields
        self.modelpath = Entry(
            self.mainFrame, width=34, background=gui_colors.lightBlue,
            foreground=gui_colors.white, borderwidth=0, insertbackground=gui_colors.white)
        self.modelpath.focus_set()
        self.modelpath.insert(0, config["modelpath"])
        self.modelpath.selection_range(0, END)
        self.modelpath.place(relx=0.495, rely=0.14, anchor=NW)
        self.modelpath.bind("<KeyRelease>", self.verifyPath)
        self.modelpath.bind("<FocusIn>", self.selectContent)

        self.settingspath = Entry(
            self.mainFrame, width=34, background=gui_colors.lightBlue,
            foreground=gui_colors.white, borderwidth=0, insertbackground=gui_colors.white)
        self.settingspath.insert(0, config["settinspath"])
        self.settingspath.place(relx=0.495, rely=0.4, anchor=NW)
        self.settingspath.bind("<KeyRelease>", self.verifyPath)
        self.settingspath.bind("<FocusIn>", self.selectContent)

        self.outDir = Entry(
            self.mainFrame, width=34, background=gui_colors.lightBlue,
            foreground=gui_colors.white, borderwidth=0, insertbackground=gui_colors.white)
        self.outDir.insert(0, config["outDir"])
        self.outDir.place(relx=0.495, rely=0.64, anchor=NW)
        self.outDir.bind("<KeyRelease>", self.verifyPath)
        self.outDir.bind("<FocusIn>", self.selectContent)
        self.outDir.bind("<FocusIn>", self.outDirFocusIn)
        self.outDir.bind("<FocusOut>", self.outDirFocusOut)

        self.verifyPath(self.settingspath)
        self.verifyPath(self.outDir)
        self.verifyPath(self.modelpath)

        # settings
        self.settingsFrame = Frame(self.master, width=650 - 190, height=100)
        self.settingsFrame["background"] = gui_colors.darkBlue
        self.settingsFrame.place(x=190, rely=0.5, anchor=W)

        self.settingsBgImage = PhotoImage(file="res/settings_bg.pgm")
        self.background = Label(
            self.settingsFrame, image=self.settingsBgImage, borderwidth=0)
        self.background.place(rely=0.5, anchor=W)

        self.settingsOkButton = ImageButton(
            self.settingsFrame, "ok", self.hideSettings)
        self.settingsOkButton.place(relx=0.925, rely=0.725, anchor=CENTER)

        self.xnormalpath = Entry(
            self.settingsFrame, width=34, background=gui_colors.lightBlue,
            foreground=gui_colors.white, borderwidth=0, insertbackground=gui_colors.white)
        self.xnormalpath.focus_set()

        self.xnormalpath.path = config["xnormalPath"]
        self.xnormalpath.insert(0, self.xnormalpath.path)
        if os.path.isfile(self.xnormalpath.path):
            self.xnormalpath.isReady = True
            self.settingsOkButton.enable()
        else:
            self.xnormalpath.isReady = False
            self.settingsOkButton.disable()

        self.xnormalpath.selection_range(0, END)
        self.xnormalpath.place(relx=0.275, rely=0.4, anchor=NW)
        self.xnormalpath.bind("<FocusIn>", self.check_xNormalPath)
        self.xnormalpath.bind("<KeyRelease>", self.check_xNormalPath)
        self.xnormalpath.bind("<Button-1>", self.selectContent)

        self.settingsWarnLabel = Label(self.settingsFrame,
                                       foreground=gui_colors.warning, bg=gui_colors.darkBlue, text="")
        self.settingsWarnLabel.place(relx=0.275, rely=0.2, anchor=NW)

        self.toggleKeepXml = ImageButtonToggle(
            self.settingsFrame, "checkbox", "checkbox_active")
        self.toggleKeepXml.place(relx=0.43, rely=0.725, anchor=CENTER)
        if config["keepXml"]:
            self.toggleKeepXml.check()

        if self.xnormalpath.isReady:
            self.hideSettings()

        # update
        self.clock = Label()
        self.tick()

    def tick(self):
        self.clock.after(100, self.tick)

        if self.modelpath.isReady and self.settingspath.isReady and self.outDir.isReady:
            if self.bakeButton.state == "DISABLED":
                self.bakeButton.enable()
        else:
            if self.bakeButton.state != "DISABLED":
                self.bakeButton.disable()

    def selectContent(self, event=None):
        event.widget.focus_set()
        event.widget.selection_range(0, END)
        # tell tk to stop handling this event
        return "break"

    def widgetIsNotReady(self, widget):
        widget.isReady = False
        widget.config(foreground=gui_colors.warning)

    def widgetIsReady(self, widget):
        widget.isReady = True
        widget.config(foreground=gui_colors.white)

    def verifyPath(self, event=None):
        # workaround for call from init
        try:
            widget = event.widget
        except AttributeError:
            widget = event
        
        path = os.path.abspath(widget.get())
        
        self.widgetIsNotReady(widget)

        if os.path.isdir(path):
            self.widgetIsReady(widget)
        elif widget == self.settingspath and not widget.isReady:
            if os.path.isfile(path) and xnormal.isXmlFile(path):
                self.widgetIsReady(widget)
        else:
            if widget == self.outDir:
                text = self.outDir.get()
                if text == "(optional)" or not text:
                    self.widgetIsReady(widget)

    def outDirFocusIn(self, event=None):
        if event.widget.get() == "(optional)":
            event.widget.delete(0, END)

    def outDirFocusOut(self, event=None):
        if event.widget.get() == "":
            self.outDir.insert(0, "(optional)")

    def showSettings(self):
        self.settingsFrame.place(x=190, rely=0.5, anchor=W)

    def hideSettings(self, doSave=True):
        self.settingsFrame.place(x=650, rely=0.5, anchor=W)

    def check_xNormalPath(self, event=None):
        widget = event.widget
        path = os.path.abspath(widget.get())

        # lets try two other paths
        xn_baseDir = os.path.join(path, "x64", "xNormal.exe")
        xn_x64Dir = os.path.join(path, "xNormal.exe")

        if os.path.exists(xn_baseDir):
            self.xnormalpath.path = xn_baseDir
        elif os.path.exists(xn_x64Dir):
            self.xnormalpath.path = xn_x64Dir
        elif os.path.exists(path) and os.path.split(path)[1] == "xNormal.exe":
            self.xnormalpath.path = path
        else:
            self.xnormalpath.path = False

        if self.xnormalpath.path:
            self.widgetIsReady(widget)
            self.settingsOkButton.enable()
            self.settingsWarnLabel.config(text="ok - found xNormal.exe", fg=gui_colors.white)
        else:
            self.widgetIsNotReady(widget)
            self.settingsOkButton.disable()
            self.settingsWarnLabel.config(text="no xNormal.exe found in directory!", fg=gui_colors.warning)

    def bake(self):
        self.warnLabel.config(text="", fg=gui_colors.warning)

        outDirPath = self.outDir.get()
        if outDirPath == "(optional)":
            outDirPath = self.modelpath.get()

        try:
            self.warnLabel.config(text="baking...")
            self.warnLabel.update()

            xnormal.path = self.xnormalpath.path
            
            retcode = xnormal.batch(
                self.settingspath.get(), self.modelpath.get(), outDirPath, self.toggleKeepXml.isChecked)
            if retcode == 0:
                self.warnLabel.config(text="bake done", fg=gui_colors.white)
            elif retcode == 1:
                self.warnLabel.config(
                    text="bake aborted", fg=gui_colors.white)
            elif retcode == -1:
                self.warnLabel.config(
                    text="error while baking", fg=gui_colors.warning)

        except Exception as e:
            self.warnLabel.config(text=e)
            if isDebug:
                raise e

    def onAppClose(self):
        try:
            print("save settings")
            config["version"] = version
            config["modelpath"] = self.modelpath.get()
            config["settinspath"] = self.settingspath.get()
            config["outDir"] = self.outDir.get()
            config["xnormalPath"] = self.xnormalpath.path
            config["keepXml"] = self.toggleKeepXml.isChecked
            with open('config.pickle', 'wb') as f:
                pickle.dump(config, f, pickle.HIGHEST_PROTOCOL)
        except:
            pass

        self.master.destroy()


def main():
    os.chdir(os.path.split(os.path.abspath(__file__))[0])

    global root, app
    root = Tk()
    root.title("Airborn Studios Backstube")
    root.wm_iconbitmap(default='res/icon.ico')
    root.minsize(650, 100)
    root.resizable(False, False)

    app = GUI(root)
    root.wm_protocol("WM_DELETE_WINDOW", app.onAppClose)
    root.mainloop()


if __name__ == '__main__':
    main()
