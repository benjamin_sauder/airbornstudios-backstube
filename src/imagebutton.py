from tkinter import *
import gui_colors

class ImageButton(Label):
    darkBlue = "#151a23"

    def __init__(self, master, imageBaseName, command = None ):
        self.images = { "NORMAL"   : PhotoImage( file="res/%s_n.pgm" %( imageBaseName ) ),
                        "HOVER"    : PhotoImage( file="res/%s_h.pgm" %( imageBaseName ) ),
                        "PRESSED"  : PhotoImage( file="res/%s_p.pgm" %( imageBaseName ) ),
                        "DISABLED" : PhotoImage( file="res/%s_d.pgm" %( imageBaseName ) ) }
        self.state = "NORMAL"
        Label.__init__(self, master, image = self.images[self.state], borderwidth = 0, highlightthickness=0, activebackground=gui_colors.darkBlue )
        self.command = command
        self.enable()

    def enable(self):
        self.state = "NORMAL"
        self.config( image = self.images[self.state] )        

        self.bind("<Button-1>", self.pressedButton )
        self.bind("<Enter>", self.hoverButton )
        self.bind("<Leave>", self.leftButton )
        self.bind("<ButtonRelease-1>", self.releasedButton )

    def disable(self):       
        self.state = "DISABLED"
        self.config( image = self.images[self.state] )

        self.unbind("<Button-1>" )
        self.unbind("<Enter>" )
        self.unbind("<Leave>" )
        self.unbind("<ButtonRelease-1>" )

    #callbacks
    def pressedButton(self, event=None):
        self.state = "PRESSED"
        self.config( image = self.images[self.state] )

    def hoverButton(self, event=None):
        self.state = "HOVER"
        self.config( image = self.images[self.state] )     

    def leftButton(self,event=None):
        self.state = "NORMAL"     
        self.config( image = self.images[self.state] )
        

    def releasedButton(self, event=None):       
        if self.state == "PRESSED" and self.command != None:
            self.command()

        self.state = "NORMAL"
        self.config( image = self.images[self.state] )

class ImageButtonToggle(ImageButton):
   
    def __init__(self, master, imageBaseName, imageBaseName_active, command = None):

        ImageButton.__init__(self, master,imageBaseName, command )

        self.unchecked_images = self.images
        self.checked_images = { "NORMAL"   : PhotoImage( file="res/%s_n.pgm" %( imageBaseName_active ) ),
                                "HOVER"    : PhotoImage( file="res/%s_h.pgm" %( imageBaseName_active ) ),
                                "PRESSED"  : PhotoImage( file="res/%s_p.pgm" %( imageBaseName_active ) ),
                                "DISABLED" : PhotoImage( file="res/%s_d.pgm" %( imageBaseName_active ) ) }
        self.isChecked = False
            
    def check(self):
        self.isChecked = True
        self.images = self.checked_images
        self.config( image = self.images[self.state] )

    def uncheck(self):
        self.isChecked = False
        self.images = self.unchecked_images
        self.config( image = self.images[self.state] )

    def releasedButton(self, event=None):                
        if self.isChecked:
            self.uncheck()
        else:
            self.check()
        super(ImageButtonToggle, self).releasedButton( event )  

        